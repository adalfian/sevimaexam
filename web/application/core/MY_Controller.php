<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
    protected $url = 'http://127.0.0.1/sevimaexam/api/';

    function __construct(){
        parent::__construct();

    }

	// request dengan curl
    protected function getContent($request, $url = null) {
        if(empty($url))
            $url = $this->url;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    public function request($data){
		$req = json_encode($data);
        $res = $this->getContent($req);
        $response = json_decode($res, TRUE);

		return $response;
	}

    public function action($data = null){
        
    }

    public function delete(){
        
    }
}
