<div class="card">
	<div class="card-header">
		Log
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-md-12">
			<?php 
			if($this->session->flashdata('error')){
				echo '
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					'.$this->session->flashdata('error').'
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				';
			}	
			if($this->session->flashdata('success')){
				echo '
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					'.$this->session->flashdata('success').'
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				';
			}	
			?>
				<div class="form-row align-items-center">
					<div class="col-sm-3 my-1">
						<label class="sr-only" for="search">Keyword</label>
						<input type="text" class="form-control" id="search" placeholder="Keyword">
					</div>
					<div class="col-sm-3 my-1">
						<button class="btn btn-sm btn-info">Cari</button>
					</div>
				</div>
				<table class="table table-striped table-bordered">
					<thead class="text-center">
						<tr>
							<th>Time</th>
							<th>Username</th>
							<th>Aktivitas</th>
							<th>IP</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no = 0;
						foreach ($list as $row) { ?>
						<tr>
							<td><?= $row['created']; ?></td>
							<td><?= $row['username']; ?></td>
							<td><?= $row['keterangan']; ?></td>
							<td><?= $row['ip']; ?></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="card-footer">
		<nav aria-label="Page navigation example">
			<ul class="pagination justify-content-center">
				<li class="page-item disabled">
				<a class="page-link" href="#" tabindex="-1">Previous</a>
				</li>
				<li class="page-item"><a class="page-link" href="#">1</a></li>
				<li class="page-item"><a class="page-link" href="#">2</a></li>
				<li class="page-item"><a class="page-link" href="#">3</a></li>
				<li class="page-item">
				<a class="page-link" href="#">Next</a>
				</li>
			</ul>
		</nav>
	</div>
</div>