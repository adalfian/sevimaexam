<h3><?= $title ?> User</h3>
<?php 
	if($this->session->flashdata('error')){
		echo '
		<div class="alert alert-danger alert-dismissible fade show" role="alert">
			'.$this->session->flashdata('error').'
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		';
	}	
	if($this->session->flashdata('success')){
		echo '
		<div class="alert alert-success alert-dismissible fade show" role="alert">
			'.$this->session->flashdata('success').'
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		';
	}	
	?>
	<?php echo validation_errors(); ?>
	<?php echo form_open('users/save'); ?>
	<table style="border:1px solid #ccc">
		<tr>
			<td>Username</td>
			<td><input type="text" name="username" value="<?= !empty($users) ? $users['username'] : '' ?>"></td>
			<td rowspan="5"><img src="<?= base_url() ?>assets/usericon.png" style="width:100px;height:100px">
				<br>
				<?php if(!empty($users)){?>
				<span class="btn btn-info btn-sm">Ubah foto</button>
				<?php }?>
			</td>
		</tr>
		<tr>
			<td>Nama</td>
			<td><input type="text" name="nama" value="<?= !empty($users) ? $users['nama'] : '' ?>"></td>
		</tr>
		<tr>
			<td>Email</td>
			<td><input type="email" name="email" value="<?= !empty($users) ? $users['email'] : '' ?>"></td>
		</tr>
		<tr>
			<td>Password</td>
			<td><input type="password" name="password" value="" <?= empty($users) ? 'required' : '' ?>></td>
		</tr>
		<tr>
			<td>Password confirm</td>
			<td><input type="password" name="passwordconf" value=""  <?= empty($users) ? 'required' : '' ?>></td>
		</tr>
		<tr>
			<td></td>
			<td><input type="checkbox" name="isadmin" <?= !empty($users) ? ($users['isadmin'] ? 'checked' : '') : '' ?>> Sebagai Admin</td>
		</tr>
		<tr>
			<td></td>
			<td><input type="checkbox" name="isactive" <?= !empty($users) ? ($users['isactive'] ? 'checked' : '') : '' ?>> Aktif</td>
		</tr>
		<tr>
			<td colspan="2"><button>Simpan</button></td>
		</tr>
	</table>
	<input type="hidden" name="iduser" value="<?= !empty($users) ? $users['iduser'] : '' ?>"">
	<input type="hidden" name="act" value="simpan">
</form>

<br>

<hr>