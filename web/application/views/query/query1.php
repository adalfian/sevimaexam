<div class="card">
	<div class="card-header">
		Query Analisis 1
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped">
					<thead class="text-center">
						<tr>
							<th>Nama</th>
							<th>Jumlah Postingan</th>
							<th>Jumlah Group</th>
							<th>Tgl. Awal Post</th>
							<th>Tgl. Akhir Post</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no = 0;
						foreach ($list as $row) { ?>
						<tr>
							<td><?= $row['name']; ?></td>
							<td><?= $row['jumlahpost']; ?></td>
							<td><?= $row['jumlahgroup']; ?></td>
							<td><?= $row['awal']; ?></td>
							<td><?= $row['akhir']; ?></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>