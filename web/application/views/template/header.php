<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
    <a class="navbar-brand" href="<?= base_url('users/') ?>">
      <img width="30px" src="<?= base_url('assets/img/logo.png') ?>">
      Sevima Exam
    </a>
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('users/') ?>">Profil</a>
        </li>
        <?php if(isAdmin()){ ?>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('users/listuser') ?>">List User</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('users/log') ?>">Log User</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('query/analisis1') ?>">Analisis 1</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('query/analisis2') ?>">Analisis 2</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('query/analisis3') ?>">Analisis 3</a>
            </li>
        <?php } ?>
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('users/logout') ?>">Logout</a>
        </li>
    </ul>
  </div>
</nav>