<html>
    <head>
        <title>Sevima Test</title>
        <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('assets/plugins/fontawesome/css/all.css') ?>">
        <link rel="icon" type="img/png" href="<?= base_url('assets/img/logo.png') ?>" sizes="16x16" />
        <script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/jquery.alphanum.js') ?>"></script>
        <script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
    </head>
    <body>
        <?php include('template/header.php') ?>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php echo $contents; ?>
                </div>
            </div>
        </div>
        <!-- Footer -->
        <footer class="page-footer font-small" style="background-color: #f8f9fa">

        <!-- Copyright -->
        <div class="footer-copyright text-center py-3">
        © 2019. Powered by <a href="#"> ADA</a>
        </div>
        <!-- Copyright -->

        </footer>
        <!-- Footer -->
    </body>
</html>