<html>
    <head>
        <title>Registrasi</title>
        <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('assets/plugins/fontawesome/css/all.css') ?>">
        <style>
        .login{
            margin-top: 30px;
        }
        </style>
    </head>
    <body>
    <div class="container">
            <div class="col-md-12 text-center">
                <h1>Registrasi</h1>
            </div>
            <div class="row">
                <div class="col-md-2">
                </div>
                <div class="col-md-8">
                    <div class="card mb-3 login">
                        <div class="card-body">
                            <?php 
                            if($this->session->flashdata('error')){
                                echo '
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    '.$this->session->flashdata('error').'
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                ';
                            }	
                            ?>
                            <?php echo validation_errors(); ?>
                            <?php echo form_open('auth/registrasi'); ?>
                                <div class="form-group">
                                    <label for="username" class="col-md-12 col-form-label">Username</label>
                                    <div class="col-md-12">
                                    <input type="text" name="username" class="form-control" id="username" placeholder="Username" value="<?php echo set_value('username'); ?>"  required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama" class="col-md-12 col-form-label">Nama</label>
                                    <div class="col-md-12">
                                    <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama lengkap anda" value="<?php echo set_value('nama'); ?>"  required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-md-12 col-form-label">Email</label>
                                    <div class="col-md-12">
                                    <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="<?php echo set_value('email'); ?>"  required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-md-12 col-form-label">Password</label>
                                    <div class="col-md-12">
                                    <input type="password" name="password" class="form-control" id="password" placeholder="Password" value="<?php echo set_value('password'); ?>"  required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-md-12 col-form-label">Konfirmasi Password</label>
                                    <div class="col-md-12">
                                    <input type="password" name="passwordconf" class="form-control" id="passwordconf" placeholder="Masukkan kembali password" value="<?php echo set_value('passwordconf'); ?>"  required>
                                    </div>
                                </div>
                                <div class="form-group  text-center">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-success btn-block"><i class="fas fa-user-plus"></i> Daftar Sekarang</button>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group  text-center">
                                    <div class="col-md-12">
                                        Sudah punya akun? 
                                        <a href="<?= base_url('auth/login') ?>" class="btn btn-info"><i class="fas fa-sign-in-alt"></i> Masuk</a>
                                    </div>
                                </div>
                                <input type="hidden" name="act" value="simpan">
                            </form>

        <script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/jquery.alphanum.js') ?>"></script>
        <script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
        <script>
        $('.alert').alert();
        $("[name='username']").alphanum({
            allowSpace: false,
            allowUpper: false
        });
        </script>
    </body>
</html>