<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	function __construct(){
		parent::__construct();

		if(cek_session()){
			redirect('users');
		}
	}

	public function index()
	{
		redirect('auth/login');
	}
    
	public function login()
	{
		$this->form_validation->set_rules('username', 'Username', 'required|trim', array('required' => '%s harap diisi!'));
		$this->form_validation->set_rules('password', 'Password', 'required|trim', array('required' => '%s harap diisi!'));
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('login');
		}
		else
		{
			$data = array();
			$data['username'] = $this->input->post('username');
			$data['password'] = $this->input->post('password');
			$data['action'] = 'login';

			$response = $this->request($data);
			if($response['success']){
				$ses = array();
				$ses['user'] = $response['data'];
				$this->session->set_userdata($ses);
				redirect('users');
			} else {
				$this->session->set_flashdata('error', $response['message']);
				redirect('auth/login');
			}
		}
	}

	public function registrasi()
	{
		if(!empty($this->input->post('act'))){
			$this->form_validation->set_rules('username', 'Username', 'required|trim', array('required' => '%s harap diisi!'));
			$this->form_validation->set_rules('nama', 'Nama', 'required|trim', array('required' => '%s harap diisi!'));
			$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email', array('required' => '%s harap diisi!'));
			$this->form_validation->set_rules('password', 'Password', 'trim');
			$this->form_validation->set_rules('passwordconf', 'Konfirmasi Password', 'trim|matches[password]');
			
			if ($this->form_validation->run() == FALSE)
			{
			  $this->load->view('register');
			}
			else
			{
			  $post = $this->input->post();
			  $act = $this->input->post('act');
	  
			  $rec = array();
			  foreach ($post as $k => $v) {
				if($k == 'act')
				  $k = 'action';
	  
				$rec[$k] = $v;
			  }
			  
			  if(!empty($rec['password'])){
					$rec['salt'] = md5(date('Y-m-d H:i:s'));
					$rec['isactive'] = '1';
					$rec['password'] = md5(md5($rec['password'].$rec['salt']).$rec['salt']);
			  }
			  
			  $response = $this->request($rec);
			  if($response['success']){
					if($_SESSION['user']['iduser'] == $response['data']['iduser']){
						$ses = array();
						$ses['user'] = $response['data'];
						$this->session->set_userdata($ses);
					}
					$this->session->set_flashdata('success', $response['message']);
					redirect('auth/login');
			  }
			  else{
				$this->session->set_flashdata('error', $response['message']);
				redirect('auth/registrasi');
			  }
			}
		} else {
			$this->load->view('register');
		}
	}
}
