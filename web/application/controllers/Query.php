<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Query extends MY_Controller {

    function __construct(){
		parent::__construct();

		if(!cek_session()){
			redirect('auth');
		}
    }

    public function index(){
        redirect('query/analisis1');
    }
    
    public function analisis1(){
        $rec = array();
        $rec['action'] = 'query1';
        $response = $this->request($rec);

        $data = array();
        $data['list'] = $response['data'];

        $this->template->load('template', 'query/query1', $data);
    }
    public function analisis2(){
        $this->template->load('template', 'query/query2');
    }
    public function analisis3(){
        $this->template->load('template', 'query/query3');
    }
}