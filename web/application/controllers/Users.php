<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {

  function __construct(){
		parent::__construct();

		if(!cek_session()){
			redirect('auth');
		}
	}

	public function index()
	{
    $user = $_SESSION['user'];

    $data = array();
    $data['users'] = array($user);

    if(!empty($this->input->post('act'))){
      $this->form_validation->set_rules('username', 'Username', 'required|trim', array('required' => '%s harap diisi!'));
      $this->form_validation->set_rules('nama', 'Nama', 'trim', 'required|trim', array('required' => '%s harap diisi!'));
      $this->form_validation->set_rules('email', 'Email', 'trim', 'required|trim', array('required' => '%s harap diisi!'));
      $this->form_validation->set_rules('password', 'Password', 'trim');
      $this->form_validation->set_rules('passwordconf', 'Konfirmasi Password', 'trim|matches[password]');
      
      if ($this->form_validation->run() == FALSE)
      {
        $this->template->load('template', 'users/profil', $data);
      }
      else
      {
        $post = $this->input->post();
        $act = $this->input->post('act');

        $rec = array();
        foreach ($post as $k => $v) {
          if($k == 'act')
            $k = 'action';

          $rec[$k] = $v;
        }

        if(!empty($rec['password'])){
          $rec['password'] = md5(md5($rec['password'].$user['salt']).$user['salt']);
        }
        
        $response = $this->request($rec);
        if($response['success']){
          if($_SESSION['user']['iduser'] == $response['data']['iduser']){
            $ses = array();
            $ses['user'] = $response['data'];
            $this->session->set_userdata($ses);
          }
          $this->session->set_flashdata('success', $response['message']);
          redirect('users');
        }
        else{
          $this->session->set_flashdata('success', $response['message']);
          redirect('users');
        }
      }
    } else {
      $this->template->load('template', 'users/profil', $data);
    }
  }
    
  public function listuser()
	{
    if(!isAdmin())
      redirect('users');

    $user = $_SESSION['user'];

    $rec = array();
    $rec['action'] = 'listuser';

    $response = $this->request($rec);

    $data = array();
    $data['users'] = array($user);
    $data['list'] = $response['data'];

    $this->template->load('template', 'users/list', $data);
  }
    
  public function detailuser($iduser)
	{
    if(!isAdmin())
      redirect('users');

    $rec = array();
    $rec['action'] = 'getuser';
    $rec['iduser'] = $iduser;
    $response = $this->request($rec);

    if($response['success']){
      $data = array();
      $data['users'] = $response['data'];
  
      $this->template->load('template', 'users/detail', $data);
    }
    else{
      redirect('users/listuser');
    }
  }
    
  public function edituser($iduser)
  {
    if(!isAdmin())
      redirect('users');

    $rec = array();
    $rec['action'] = 'getuser';
    $rec['iduser'] = $iduser;
    $response = $this->request($rec);

    if($response['success']){
      $data = array();
      $data['users'] = $response['data'];
      $data['title'] = 'Edit';
  
      $this->template->load('template', 'users/view', $data);
    }
    else{
      redirect('users/listuser');
    }
  }

  public function adduser()
  {
    if(!isAdmin())
      redirect('users');

    $user = $_SESSION['user'];

    $data = array();
    $data['title'] = 'Add';

    $this->template->load('template', 'users/view', $data);
  }

  public function save(){
    if(!empty($this->input->post('act'))){
      $iduser = $this->input->post('iduser');
      $this->form_validation->set_rules('username', 'Username', 'required|trim', array('required' => '%s harap diisi!'));
      $this->form_validation->set_rules('nama', 'Nama', 'trim', 'required|trim', array('required' => '%s harap diisi!'));
      $this->form_validation->set_rules('email', 'Email', 'trim', 'required|trim', array('required' => '%s harap diisi!'));
      $this->form_validation->set_rules('password', 'Password', (empty($iduser) ? 'required|' : '').'trim');
      $this->form_validation->set_rules('passwordconf', 'Konfirmasi Password', (empty($iduser) ? 'required|' : '').'trim|matches[password]');
      
      if ($this->form_validation->run() == FALSE)
      {
        if(empty($iduser)){
          $this->session->set_flashdata('error', validation_errors());
          redirect('users/adduser');
        }else{
          $this->session->set_flashdata('error', validation_errors());
          redirect('users/edituser/'.$iduser);
        }
      }
      else
      {
        $post = $this->input->post();
        $act = $this->input->post('act');

        if(!empty($iduser)){
          $recuser= array();
          $recuser['action'] = 'getuser';
          $recuser['iduser'] = $iduser;
          $response = $this->request($recuser);
          if($response['success']){
            $user = $response['data'];
          }
          else{
            $this->session->set_flashdata('error', 'User tidak ditemukan');
            redirect('users/edituser/'.$iduser);
          }
        }

        $rec = array();
        foreach ($post as $k => $v) {
          if($k == 'act')
            $k = 'action';

          $rec[$k] = $v;
        }

        if(empty($post['isadmin']))
          $rec['isadmin'] = '0';
        else
          $rec['isadmin'] = '1';

        if(empty($post['isactive']))
          $rec['isactive'] = '0';
        else
          $rec['isactive'] = '1';

        if(!empty($rec['password'])){
          if(empty($iduser))
            $rec['salt'] =  md5(date('Y-m-d H:i:s'));
          else{
            $rec['salt'] = $user['salt'];
          }
          $rec['password'] = md5(md5($rec['password'].$rec['salt']).$rec['salt']);
        }
        
        $response = $this->request($rec);
        if($response['success']){
          if($_SESSION['user']['iduser'] == $response['data']['iduser']){
            $ses = array();
            $ses['user'] = $response['data'];
            $this->session->set_userdata($ses);
          }
          $this->session->set_flashdata('success', $response['message']);
          redirect('users/listuser');
        }
        else{
          $this->session->set_flashdata('error', $response['message']);
          redirect('users/listuser');
        }
      }
    } else {
      redirect('users/listuser'); 
    }
  }

  public function deleteuser($iduser){
    if(!isAdmin())
      redirect('users');

    $rec = array();
    $rec['action'] = 'getuser';
    $rec['iduser'] = $iduser;
    $response = $this->request($rec);

    if($response['success']){
      $rechapus = array();
      $rechapus['action'] = 'deleteuser';
      $rechapus['iduser'] = $iduser;
      $responsehapus = $this->request($rechapus);
      
      if($responsehapus['success']){
        $this->session->set_flashdata('success', $response['message']);
        redirect('users/listuser');
      }
      else{
        $this->session->set_flashdata('error', $response['message']);
        redirect('users/listuser');
      }
    }
    else{
      $this->session->set_flashdata('error', $response['message']);
      redirect('users/listuser');
    }
  }

  public function log(){
    if(!isAdmin())
      redirect('users');

    $rec = array();
    $rec['action'] = 'listlog';

    $response = $this->request($rec);
    if(empty($response['data']))
      $response['data'] = array();

    $data = array();
    $data['list'] = $response['data'];

    $this->template->load('template', 'users/log', $data);
  }

  public function logout(){
    unset($_SESSION['user']);
    redirect('auth');
  }
}
