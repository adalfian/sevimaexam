<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MApi_usertoken extends MY_Model {
    protected $table = 'usertoken_alfian';
    protected $schema = 'test';
    protected $key = 'token';

    
    public function get($data){
        $where = $this->getCondition($data);
        
        $this->db->where($where);
        return $this->db->get($this->getTable());
    }
}
