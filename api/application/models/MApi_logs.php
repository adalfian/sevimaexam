<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MApi_logs extends MY_Model {
    protected $table = 'logs_alfian';
    protected $schema = 'test';
    protected $key = 'idlog';

    
    public function get($data){
        $where = $this->getCondition($data);
        
        $this->db->where($where);
        return $this->db->get($this->getTable());
    }

    public function listlog(){
        return $this->db->get($this->getTable());
    }
}
