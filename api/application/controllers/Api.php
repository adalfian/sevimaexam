<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MY_Controller {

	function __construct(){
		parent::__construct();
	
		$this->load->model('MApi_users');
		$this->load->model('MApi_logs');
		$this->load->model('MApi_usertoken');
		$this->load->model('MApi');
	}

	public function index()
	{
		$datajson = file_get_contents('php://input');
		$data = json_decode($datajson, true);

		$response = array();
		if(!empty($data['action'])){
			switch ($data['action']) {
				case 'login':
					unset($data['action']);
					list($ok, $msg, $info) = $this->login($data);
					break;
				case 'simpan':
					unset($data['action']);
					list($ok, $msg, $info) = $this->simpan($data);
					break;
				case 'listuser':
					unset($data['action']);
					list($ok, $msg, $info) = $this->listuser();
					if(!empty($info))
						$info = $info->result_array();
					break;
				case 'getuser':
					unset($data['action']);
					list($ok, $msg, $info) = $this->getuser($data);
					break;
				case 'listlog':
					unset($data['action']);
					list($ok, $msg, $info) = $this->listlog();
					if(!empty($info))
						$info = $info->result_array();
					break;
				case 'deleteuser':
					unset($data['action']);
					list($ok, $msg) = $this->deleteuser($data);
					break;
				case 'query1':
					unset($data['action']);
					list($ok, $msg, $info) = $this->query1();
					if(!empty($info))
						$info = $info->result_array();
					break;
				default:
					$ok = false;
					$msg = 'Action not found.';
					break;
			}
			$response['success'] = $ok;
			$response['message'] = $msg;
			if(!empty($info))
				$response['data'] = $info;
		} else {
			$response['success'] = false;
			$response['message'] = 'Action not found.';
		}

		header("Access-Control-Allow-Orgin: *");
		header("Access-Control-Allow-Methods: *");
		header("Content-Type: application/json");

		echo json_encode($response);
	}

	public function login($data){
		$password = $data['password'];
		unset($data['password']);

		$user = $this->MApi_users->get($data)->row_array();

		if(empty($user)){
			$log = array();
			$log['ip'] = $_SERVER['REMOTE_ADDR'];
			$log['created'] = date('Y-m-d H:i:s');
			$log['keterangan'] = 'Login - User tidak ditemukan';
			$log['username'] = $data['username'];
			$this->MApi_logs->insert($log);
		}

		if($user['password'] != md5(md5($password.$user['salt']).$user['salt'])){
			$log = array();
			$log['ip'] = $_SERVER['REMOTE_ADDR'];
			$log['created'] = date('Y-m-d H:i:s');
			$log['keterangan'] = 'Login - Password tidak sesuai';
			$log['iduser'] = $user['iduser'];
			$log['username'] = $user['username'];
			$this->MApi_logs->insert($log);

			unset($user);
		}

		if(isset($user) and $user['isactive']!='1'){
			$msg = 'User anda sudah tidak aktif';

			$log = array();
			$log['ip'] = $_SERVER['REMOTE_ADDR'];
			$log['created'] = date('Y-m-d H:i:s');
			$log['keterangan'] = 'Login - '.$msg;
			$log['iduser'] = $user['iduser'];
			$log['username'] = $user['username'];
			$this->MApi_logs->insert($log);

			unset($user);
		}
		
		if(empty($user)){
			return array(false, (!empty($msg) ? $msg : 'Username/Password salah'), null);
		}
		else{
			$log = array();
			$log['ip'] = $_SERVER['REMOTE_ADDR'];
			$log['created'] = date('Y-m-d H:i:s');
			$log['keterangan'] = 'Login - Berhasil';
			$log['iduser'] = $user['iduser'];
			$log['username'] = $user['username'];
			$this->MApi_logs->insert($log);

			return array(true, 'Login Berhasil', $user);
		}
	}

	public function simpan($data){
		$info = '';
		$username = $data['username'];
		
		if(!empty($data['iduser'])){
			$key = $data['iduser'];
			$ok = $this->MApi_users->update($data, $key);

			if($ok){
				$info = $this->MApi_users->get(array('iduser' => $key))->row_array();

				$log = array();
				$log['ip'] = $_SERVER['REMOTE_ADDR'];
				$log['created'] = date('Y-m-d H:i:s');
				$log['keterangan'] = 'Update user';
				$log['iduser'] = $key;
				$this->MApi_logs->insert($log);
			}
		}
		else{
			$user = $this->MApi_users->get(array('username' => $username))->row_array();
			if(empty($user)){
				list($ok, $key) = $this->MApi_users->insert($data, true);
				if($ok){
					$info = $this->MApi_users->get(array('iduser' => $key))->row_array();

					$log = array();
					$log['ip'] = $_SERVER['REMOTE_ADDR'];
					$log['created'] = date('Y-m-d H:i:s');
					$log['keterangan'] = 'Insert user';
					$log['iduser'] = $key;
					$this->MApi_logs->insert($log);
				}
			}
			else{
				return array(false, 'Username sudah dipakai', $info);
			}
		}

		return array($ok, 'Simpan data user ' . ($ok ? 'berhasil' : 'gagal'), $info);
	}

	public function listuser(){
		$data = $this->MApi_users->listuser();

		return array((!empty($data) ? true : false), 'List User ' . (!empty($data) ? 'berhasil' : 'gagal'), $data);
	}

	public function listlog(){
		$data = $this->MApi_logs->listlog();

		return array((!empty($data) ? true : false), 'List Log ' . (!empty($data) ? 'berhasil' : 'gagal'), $data);
	}

	public function getuser($data){
		$user = $this->MApi_users->get($data)->row_array();

		if(empty($user))
			return array(false, 'User tidak ditemukan', null);
		else
			return array(true, 'User ditemukan', $user);
	}

	public function deleteuser($data){
		$iduser = $data['iduser'];
		$ok = $this->MApi_users->delete($iduser);

		$log = array();
		$log['ip'] = $_SERVER['REMOTE_ADDR'];
		$log['created'] = date('Y-m-d H:i:s');
		$log['keterangan'] = 'Hapus user';
		$log['iduser'] = $iduser;
		$this->MApi_logs->insert($log);

		return array($ok, ($ok ? 'Berhasil' : 'Gagal').' hapus user');
	}

	public function query1(){
		$data = $this->MApi->query1();

		return array(true, 'Sukses', $data);
	}
}
