<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

    function __construct(){
        parent::__construct();

    }

	public function listfields(){
        
        $query = $this->db->query('SELECT * FROM ' . $this->getTable() . ' limit 1');
        
        $fields = array();
        foreach ($query->list_fields() as $field) {
            $fields[] = $field;
        }

        return $fields;
    }

    public function getTable($table = null){
        if(!empty($table))
            $this->table = $table;
        
        if(!empty($this->schema) and empty($table)){
            return $this->schema . '.' .$this->table;
        }

        return $this->table;
    }

    public function getKey(){
        $a_key = explode(',', $this->key);
        
		foreach ($a_key as $k => $v) {
            $a_key[$k] = trim($v);
        }

        if(count($a_key) == 1)
            return $this->key;
        else
		    return $a_key;
    }
    
    // get key from row/data
    public function getPagerKey($row, $separator = '/'){
        $keycol = $this->getKey();

        if (!is_array($keycol))
            $keycol = array($keycol);        
        
        foreach ($keycol as $key) {
            if(!empty($row[$key]))
                $a_key[] = $row[$key];
        }

        return implode($separator, $a_key);
    }

    public function insert($record, $returning = false){
        $fields = $this->listfields();

        $rec = array();
        foreach ($fields as $field) {
            if(!empty($record[$field])){
                $rec[$field] = $record[$field];
            }
        }

        if($returning){
            $err = $this->db->insert($this->getTable(), $rec);
            return array($err, $this->db->insert_id());
        }else{
            return $this->db->insert($this->getTable(), $rec);
        }
    }

    public function update($record, $key, $returning = false){
        $keys = explode('/', $key);
        $a_key = $this->getKey();
        $fields = $this->listfields();

        $where = array();

        if(!is_array($a_key))
            $a_key = array($a_key);

        foreach ($a_key as $k => $v) {
            $where[$v] = $keys[$k];
        }

        $rec = array();
        foreach ($fields as $field) {
            if(isset($record[$field]) and $record[$field]=='0')
                $rec[$field] = $record[$field];

            if(!empty($record[$field]))
                $rec[$field] = $record[$field];
        }

        $this->db->where($where);
        $data = $this->db->get($this->getTable())->row_array();

        // cek perbedaan value di table
        foreach ($rec as $x => $row) {
            if($row == $data[$x])
                unset($rec[$x]);
        }
       
        if(!empty($rec)){
            $this->db->set($rec);
            $this->db->where($where);
            if($returning)
                return array($this->db->update($this->getTable()), $this->getPagerKey($data));
            else
                return $this->db->update($this->getTable());
        }
        else{
            if($returning)
                return array(true, $this->getPagerKey($data));
            else
                return true;
        }
    }

    public function delete($key){
        $keys = explode('/', $key);
        $a_key = $this->getKey();

        $where = array();

        if(!is_array($a_key))
            $a_key = array($a_key);

        foreach ($a_key as $k => $v) {
            $where[$v] = $keys[$k];
        }

        $this->db->where($where);
        return $this->db->delete($this->getTable());
    }

    public function get($key){
        $keys = explode('/', $key);
        $a_key = $this->getKey();

        $where = array();

        if(!is_array($a_key))
            $a_key = array($a_key);

        foreach ($a_key as $k => $v) {
            $where[$v] = $keys[$k];
        }

        $this->db->where($where);
        return $this->db->get($this->getTable());
    }

    public function getCondition($where = array()){
        $cond = array();
        foreach ($where as $k => $v) {
            $cond[$k] = $v;
        }

        return $cond;
    }
}
